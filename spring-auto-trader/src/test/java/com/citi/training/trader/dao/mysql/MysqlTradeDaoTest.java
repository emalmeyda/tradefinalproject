package com.citi.training.trader.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.CreateStrategyDao;
import com.citi.training.trader.dao.SimpleStrategyDao;
import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.dao.TradeDao;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlTradeDaoTest {

	@Autowired
	TradeDao tradeDao;
	
	@Autowired
	StockDao stockDao;
	
	@Autowired
	SimpleStrategyDao simpleStrategyDao;
	
	@Test
	public void test_save_and_findAll(){
		
		// insert stock
//		Stock stock = new Stock(1, "AAPL");
//		stockDao.create(stock);
//		
		int id = -1;
//		double price = 123.0;
//		int size = 100;
//        TradeType tradeType = TradeType.BUY;
//        TradeState tradeState = TradeState.FILLED;
//        LocalDateTime lastStateChange = LocalDateTime.now();
//        SimpleStrategy strategy = new SimpleStrategy(1, stock, 100,
//                100.0, 0,
//                100.0, 150.0,
//                new Date());
//        int algoType = 1;
//        int strat = 1;
//        
//        Trade trade = new Trade(id, stock, price, size, tradeType,
//				tradeState, lastStateChange, strategy, algoType, strat);
//        
//		tradeDao.save(trade);
//		assertThat(tradeDao.findAll().size(), equalTo(1));
	}
	
//	@Test
//	public void test_findById() {
//		
//		// insert stock
//		Stock stock = new Stock(1, "AAPL");
//		stockDao.create(stock);
//		
//		
//		
//		int id = -1;
//		double price = 123.0;
//		int size = 100;
//        TradeType tradeType = TradeType.BUY;
//        TradeState tradeState = TradeState.FILLED;
//        LocalDateTime lastStateChange = LocalDateTime.now();
//        SimpleStrategy strategy = new SimpleStrategy(-1, stock, 100,
//                100.0, 0,
//                100.0, 150.0,
//                new Date());
//        int algoType = 1;
//        
//        //Map<String, String> map = new HashMap<String, String>();
//        
//        simpleStrategyDao.save(strategy);
//        
//        //createStrategyDao.createStrategy(strategy);
//        
//        Trade trade = new Trade(id, stock, price, size, tradeType,
//				tradeState, lastStateChange, strategy, algoType, 0);
//        
//		tradeDao.save(trade);
//		assertThat(tradeDao.findById(1), equalTo(trade));
//		
//	}
//	
//	@Test
//	public void findAllByState(Trade.TradeState state) {}
//		
//	@Test
//	public void deleteById(int id) {}
	
	
	
	
	
	
}
