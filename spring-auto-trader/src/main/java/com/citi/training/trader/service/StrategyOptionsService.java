package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.StrategyOptionsDao;
import com.citi.training.trader.model.StrategyOptions;

@Component
public class StrategyOptionsService {
	@Autowired
	StrategyOptionsDao strategyOptionsDao;
	
	public List<StrategyOptions> findAll(){
		return strategyOptionsDao.findAll();
	}
	
	public String create(StrategyOptions StrategyOptions){
		return strategyOptionsDao.create(StrategyOptions);
	}
	public StrategyOptions findById(String id) {
		return strategyOptionsDao.findById(id);
	}
	public void deleteById(String id) {
		strategyOptionsDao.deleteById(id);
	}
}
