package com.citi.training.trader.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.StrategyStateDao;
import com.citi.training.trader.model.MovingAverages;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;

@Component
public class MysqlStateStrategyDao implements StrategyStateDao {

	
	private static final Logger logger =
            LoggerFactory.getLogger(MysqlStateStrategyDao.class);
	
		
	private static String GET_PL = "select simple_strategy.id as strategy_id, stock.id as stock_id, stock.ticker, " +
            "size, exit_profit_loss, current_position, last_trade_price, profit, stopped " +
            "from simple_strategy left join stock on stock.id = simple_strategy.stock_id";
	
	private static String GET_PLMA = "select moving_averages.id as strategy_id, stock.id as stock_id, stock.ticker, " +
            "size, exit_profit_loss, short_ma_seconds, long_ma_seconds, lastShortMA, lastLongMA, current_position, last_trade_price, profit, trade_pl, stopped " +
            "from moving_averages left join stock on stock.id = moving_averages.stock_id";
	
	
	@Autowired
    private JdbcTemplate tpl;
	
	
	@Override
	public List<SimpleStrategy> getPL() {
		logger.debug("getPL SQL: [" + GET_PL + "]");
		return tpl.query(GET_PL,
                new SimpleStrategyMapper());
	}
	
	@Override
	public List<MovingAverages> getPLMA() {
		logger.debug("getPLMA SQL: [" + GET_PLMA + "]");
		return tpl.query(GET_PLMA,
                new MovingAveragesMapper());

	}

	

	private static final class SimpleStrategyMapper implements RowMapper<SimpleStrategy> {
        public SimpleStrategy mapRow(ResultSet rs, int rowNum) throws SQLException {
            logger.debug("Mapping simple_strategy result set row num [" + rowNum + "], id : [" +
                         rs.getInt("strategy_id") + "]");

            return new SimpleStrategy(rs.getInt("strategy_id"),
                             new Stock(rs.getInt("stock_id"),
                                       rs.getString("stock.ticker")),
                             rs.getInt("size"),
                             rs.getDouble("exit_profit_loss"),
                             rs.getInt("current_position"),
                             rs.getDouble("last_trade_price"),
                             rs.getDouble("profit"),
                             rs.getDate("stopped"));
        }
    }
	
	private static final class MovingAveragesMapper implements RowMapper<MovingAverages> {
        public MovingAverages mapRow(ResultSet rs, int rowNum) throws SQLException {
        	logger.debug("Mapping moving_averages result set row num [" + rowNum + "], id : [" +
                    rs.getInt("strategy_id") + "]");

	       return new MovingAverages(rs.getInt("strategy_id"),
	                        new Stock(rs.getInt("stock_id"),
	                                  rs.getString("stock.ticker")),
	                        rs.getInt("size"),
	                        rs.getDouble("exit_profit_loss"),
	                        rs.getInt("short_ma_seconds"),
	                        rs.getInt("long_ma_seconds"),
	                        rs.getDouble("lastShortMA"),
	                        rs.getDouble("lastLongMA"),
	                        rs.getInt("current_position"),
	                        rs.getDouble("last_trade_price"),
	                        rs.getDouble("profit"),
	                        rs.getInt("trade_pl"),
	                        rs.getDate("stopped"));	
        }
    }


	
}
