package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.MovingAverages;

public interface MovingAveragesDao {
	
	 List<MovingAverages> findAll();

	 int save(MovingAverages mas);
	 
}
