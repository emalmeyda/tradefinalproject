package com.citi.training.trader.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.CreateStrategyDao;

@Component
public class CreateStrategyService {
	
	@Autowired
	private CreateStrategyDao createStrategyDao;
	
	public void createStrategy(Map trade) {
		createStrategyDao.createStrategy(trade);
	}
	
	public void stopTrade(Map trade) {
		createStrategyDao.stopTrade(trade);
	}
}
