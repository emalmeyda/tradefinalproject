package com.citi.training.trader.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.CreateStrategyDao;
import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

@Component
public class MysqlCreateStrategyDao implements CreateStrategyDao {

	private static final Logger logger =
            LoggerFactory.getLogger(MysqlCreateStrategyDao.class);
	
	private static String CHECK_STOCK = "select count(ticker) from stock where ticker = ?";
	
	private static String INSERT_STOCK = "insert into stock (ticker) values (:ticker)";
	
	private static String INSERT_SIMPLE_STRATEGY = "insert into simple_strategy (stock_id, size, exit_profit_loss)"
			+ "values (:stock_id, :size, :exit_profit_loss)";
	
	private static String INSERT_MOVING_AVERAGES = "insert into moving_averages" 
			+ "(stock_id, size, exit_profit_loss, short_ma_seconds, long_ma_seconds, trade_pl)"
			+"values (:stock_id, :size, :exit_profit_loss, :short_ma_seconds, :long_ma_seconds, :trade_pl)";
	
	private static String STOP_STRATEGY_SIMPLE = "update simple_strategy set stopped = current_timestamp() where id = :strategyID";
	
	private static String STOP_STRATEGY_MA = "update moving_averages set stopped = current_timestamp() where id = :strategyID";


	Stock stock = null;
	
	
	@Autowired
	StockDao stockDao;
	
	@Autowired
	private JdbcTemplate tpl;
	
	@Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	@Override
	public void createStrategy(Map trade) {
//		System.out.println(trade.get("strategy"));
		
		if (trade.get("strategy").equals("simple_strategy")) {
				//System.out.println("in simple");
			
			//check if stock exists first
			int stockExists = tpl.queryForObject(CHECK_STOCK, new Object[] {trade.get("ticker").toString()},
								Integer.class);
			
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
            KeyHolder keyHolder = new GeneratedKeyHolder();

            
            
            // stock not already in table
			if (stockExists == 0) {
				//insert stock into db
				MapSqlParameterSource stockInsertParameters = new MapSqlParameterSource();
				
				stockInsertParameters.addValue("ticker", trade.get("ticker").toString());
				namedParameterJdbcTemplate.update(INSERT_STOCK, stockInsertParameters, keyHolder);
				
				logger.debug("createStrategy SQL: [" + INSERT_SIMPLE_STRATEGY + "]");
				stock = stockDao.findByTicker(trade.get("ticker").toString());

				
				// take on insert query
	            
		        namedParameters.addValue("stock_id", stock.getId());
		        namedParameters.addValue("size", trade.get("shareCount"));
		        namedParameters.addValue("exit_profit_loss", trade.get("Exit profit loss"));
		        
	            namedParameterJdbcTemplate.update(INSERT_SIMPLE_STRATEGY, namedParameters, keyHolder);

				
			}
			// stock not already in table
			else if (stockExists >= 1) {
				stock = stockDao.findByTicker(trade.get("ticker").toString());
				logger.debug("createStrategy SQL: [" + INSERT_SIMPLE_STRATEGY + "]");
				
				// take on insert query
	            
		        namedParameters.addValue("stock_id", stock.getId());
		        namedParameters.addValue("size", trade.get("shareCount"));
		        namedParameters.addValue("exit_profit_loss", trade.get("Exit profit loss"));
		        
	            namedParameterJdbcTemplate.update(INSERT_SIMPLE_STRATEGY, namedParameters, keyHolder);

			}			
		}
		else if (trade.get("strategy").equals("moving_averages")) {
			
			
//			System.out.println("in moving average");
			
			//check if stock exists first
			int stockExists = tpl.queryForObject(CHECK_STOCK, new Object[] {trade.get("ticker").toString()},
								Integer.class);
			
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
            KeyHolder keyHolder = new GeneratedKeyHolder();

            
            
            // stock not already in table
			if (stockExists == 0) {
				//insert stock into db
//				System.out.println("Stock not in table");				

				MapSqlParameterSource stockInsertParameters = new MapSqlParameterSource();
				
				stockInsertParameters.addValue("ticker", trade.get("ticker").toString());
				namedParameterJdbcTemplate.update(INSERT_STOCK, stockInsertParameters, keyHolder);
				
				logger.debug("createStrategy SQL: [" + INSERT_MOVING_AVERAGES + "]");
				stock = stockDao.findByTicker(trade.get("ticker").toString());
				
				
				// take on insert query
		        namedParameters.addValue("stock_id", stock.getId());
		        namedParameters.addValue("size", trade.get("shareCount"));
		        namedParameters.addValue("exit_profit_loss", trade.get("Exit profit loss"));
		        namedParameters.addValue("short_ma_seconds", trade.get("Short moving average (seconds)"));
		        namedParameters.addValue("long_ma_seconds", trade.get("Long moving average (seconds)"));
		        namedParameters.addValue("trade_pl", trade.get("Trade profit loss"));
		        
	            namedParameterJdbcTemplate.update(INSERT_MOVING_AVERAGES, namedParameters, keyHolder);

				
			}
			// stock already in table
			else if (stockExists >= 1) {
//				System.out.println("Stock in table");				

				stock = stockDao.findByTicker(trade.get("ticker").toString());
				logger.debug("createStrategy SQL: [" + INSERT_MOVING_AVERAGES + "]");
				
				// take on insert query
	            
				namedParameters.addValue("stock_id", stock.getId());
		        namedParameters.addValue("size", trade.get("shareCount"));
		        namedParameters.addValue("exit_profit_loss", trade.get("Exit profit loss"));
		        namedParameters.addValue("short_ma_seconds", trade.get("Short moving average (seconds)"));
		        namedParameters.addValue("long_ma_seconds", trade.get("Long moving average (seconds)"));
		        namedParameters.addValue("trade_pl", trade.get("Trade profit loss"));
		        
	            namedParameterJdbcTemplate.update(INSERT_MOVING_AVERAGES, namedParameters, keyHolder);

			}	
			
		}
		
		logger.debug("Trade Entered in create strategy");
	}


	@Override
	public void stopTrade(Map trade) {
		if (trade.get("strategy").equals("simple_strategy")) {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
            KeyHolder keyHolder = new GeneratedKeyHolder();
            
            namedParameters.addValue("strategyID", trade.get("strategyID"));
            
            namedParameterJdbcTemplate.update(STOP_STRATEGY_SIMPLE, namedParameters, keyHolder);

		}
		else if ((trade.get("strategy").equals("moving_averages"))){
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
            KeyHolder keyHolder = new GeneratedKeyHolder();
            
            namedParameters.addValue("strategyID", trade.get("strategyID"));
            namedParameterJdbcTemplate.update(STOP_STRATEGY_MA, namedParameters, keyHolder);
		}
	}
	
	
}
