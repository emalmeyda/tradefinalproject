package com.citi.training.trader.messaging;

import java.util.concurrent.TimeUnit;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.TradeMoving;
import com.citi.training.trader.service.TradeService;

@Component
public class TradeSender {
	
	private final static Logger logger =
            LoggerFactory.getLogger(TradeSender.class);
	
	private static String GET_TRADE_STATUS = "select state from trade where id = (select max(id) from trade)";

	@Autowired
	private TradeService tradeService;
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Autowired
    private JdbcTemplate tpl;
	
	@Value("${jms.sender.responseQueueName:OrderBroker_Reply}")
	private String responseQueue;
	
	public void sendTrade(Trade tradeToSend) {
		logger.debug("Sending Trade " + tradeToSend);
		logger.debug("Trade XML:");
		logger.debug(Trade.toXml(tradeToSend));
		
		tradeToSend.stateChange(Trade.TradeState.WAITING_FOR_REPLY);
		tradeService.save(tradeToSend);
		
		jmsTemplate.convertAndSend("OrderBroker", Trade.toXml(tradeToSend), message -> {
		    message.setStringProperty("Operation", "update");
		    message.setJMSCorrelationID(String.valueOf(tradeToSend.getId()));
		    message.setJMSReplyTo(buildReplyTo());
		    return message;
		});
		
		logger.debug("trade sender loop SQL: [" + GET_TRADE_STATUS + "]");
		 
		 while (tpl.queryForObject(GET_TRADE_STATUS, String.class).equals("WAITING_FOR_REPLY")) {
			 try {
				 //System.out.println("waiting for one second");
 				TimeUnit.SECONDS.sleep(1);
 			} catch (InterruptedException e) {
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}
  			 //System.out.println("we are waiting!!!!");
		 }
		 //System.out.println("result is: not waiting" );
	}
	
	public void sendTrade(TradeMoving tradeToSend) {
        logger.debug("Sending Trade " + tradeToSend);
        logger.debug("TradeMoving XML:");
        logger.debug(TradeMoving.toXml(tradeToSend));
        
        tradeToSend.stateChange(TradeMoving.TradeState.WAITING_FOR_REPLY);
		tradeService.save(tradeToSend);
        
        jmsTemplate.convertAndSend("OrderBroker", TradeMoving.toXml(tradeToSend), message -> {
            message.setStringProperty("Operation", "update");
            message.setJMSCorrelationID(String.valueOf(tradeToSend.getId())); // concat trade id 
            return message;
        });
        
        
     // wait until trade is now rejected or filled
		 logger.debug("trade sender loop SQL: [" + GET_TRADE_STATUS + "]");
		 
		 while (tpl.queryForObject(GET_TRADE_STATUS, String.class).equals("WAITING_FOR_REPLY")) {
			 try {
				 //System.out.println("waiting for one second");
  				TimeUnit.SECONDS.sleep(1);
  			} catch (InterruptedException e) {
  				// TODO Auto-generated catch block
  				e.printStackTrace();
  			}
   			 //System.out.println("we are waiting!!!!");
		 }
		 //System.out.println("result is: not waiting" );
        
	}
	
	private Destination buildReplyTo() throws JMSException {
		final Session session = jmsTemplate.getConnectionFactory().createConnection()
		    .createSession(false, Session.AUTO_ACKNOWLEDGE);
		final Destination queue =
		    jmsTemplate.getDestinationResolver().resolveDestinationName(session, responseQueue, false);
		return queue;
	}

}
