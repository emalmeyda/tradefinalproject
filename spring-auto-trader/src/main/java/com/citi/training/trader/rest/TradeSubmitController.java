package com.citi.training.trader.rest;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.dao.CreateStrategyDao;

@RestController
@RequestMapping("${com.citi.training.trader.rest.trade-base-path:/tradeSubmit}")
@CrossOrigin(origins = "http://localhost:4200")
public class TradeSubmitController {

	// @Autowired
	// TradeSubmitService TradeSubmitService;

	private static final Logger logger = LoggerFactory.getLogger(TradeSubmitController.class);

	@Autowired
	CreateStrategyDao createStrategyDao;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void findAll() {
		logger.info("Running GET method findAll for tradeINFO");
		// return TradeSubmitService.findAll();
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String create(@RequestBody Map<String, String> trade) {
		logger.info("create(" + trade + ")");
		//Write code strategy here
		logger.info("created trade: " + trade);

		if (trade.get("strategy").equals("simple_strategy")) {
//			System.out.println("Its a simple strategy");
			//System.out.println("enter simple trade");
			createStrategyDao.createStrategy(trade);
		}
		else if (trade.get("strategy").equals("moving_averages")) {
//			System.out.println("Its a moving average strategy");
			createStrategyDao.createStrategy(trade);

		}

		return "success";

		//return new ResponseEntity<Trade>(trade, HttpStatus.CREATED);
	}
	@RequestMapping(value="/stop", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String stopTrade(@RequestBody Map<String, String> trade) {
		logger.info("stop(" + trade + ")");
		//Write code strategy here
		logger.info("stop trade: " + trade);
		
		//System.out.println(trade);
		
		if (trade.get("strategy").equals("simple_strategy")) {
//			System.out.println("Its a simple strategy");
			//System.out.println("enter simple trade");
			createStrategyDao.stopTrade(trade);
		}
		else if (trade.get("strategy").equals("moving_averages")) {
//			System.out.println("Its a moving average strategy");
			createStrategyDao.stopTrade(trade);

		}
		
		
		return "success";

		//return new ResponseEntity<Trade>(trade, HttpStatus.CREATED);
	}
}
