package com.citi.training.trader.dao;

import java.util.Map;

public interface CreateStrategyDao {
	
	void createStrategy(Map trade);
	
	void stopTrade(Map trade);

}
