package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.StrategyOptions;

public interface StrategyOptionsDao {
	List<StrategyOptions> findAll();
	String create(StrategyOptions StrategyOptions);
	StrategyOptions findById(String id);
	void deleteById(String id);
}