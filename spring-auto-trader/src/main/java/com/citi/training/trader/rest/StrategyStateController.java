package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.MovingAverages;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.service.StrategyStateService;

@RestController
@RequestMapping("${com.citi.training.trader.rest.stock-base-path:/strategy-details}")
@CrossOrigin("*")
public class StrategyStateController {
	
	 private static final Logger logger =
             LoggerFactory.getLogger(StrategyStateController.class);
	 
	 @Autowired
	 private StrategyStateService strategyStateService;
	 
	 @RequestMapping(value="/simple_strategy", method=RequestMethod.GET,
             produces=MediaType.APPLICATION_JSON_VALUE)
	 public List<SimpleStrategy> getPL() {
		logger.debug("getPL()");
		return strategyStateService.getPL();
	 }
	 
	 @RequestMapping(value="/moving_averages", method=RequestMethod.GET,
             produces=MediaType.APPLICATION_JSON_VALUE)
	 public List<MovingAverages> getPLMA() {
		logger.debug("getPL()");
		return strategyStateService.getPLMA();
	 }

}