package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.MovingAverages;
import com.citi.training.trader.model.SimpleStrategy;

public interface StrategyStateDao {
	
	List<SimpleStrategy> getPL();
	
	List<MovingAverages> getPLMA();

}
