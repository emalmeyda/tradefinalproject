package com.citi.training.trader.model;

import java.io.StringReader;
import java.io.StringWriter;
import java.time.LocalDateTime;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement(name = "trade")
@XmlAccessorType(XmlAccessType.FIELD)

// Ignore properties we created for Xml marshalling
@JsonIgnoreProperties({ "tradeTypeXml", "stateXml", "lastStateChangeXml" })
public class TradeMoving {
    private static final Logger log = LoggerFactory.getLogger(TradeMoving.class);

    // Static objects for marshalling to/from XML
    private static JAXBContext jaxbContext = null;
    private static Unmarshaller unmarshaller = null;
    private static Marshaller marshaller = null;

    static {
        try {
            jaxbContext = JAXBContext.newInstance(TradeMoving.class);
            unmarshaller = jaxbContext.createUnmarshaller();
            marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        } catch (JAXBException e) {
            log.error("Unable to create TradeMoving JAXB elements");
        }
    }

    public enum TradeType {
        BUY("true"), SELL("false");

        private final String xmlString;

        private TradeType(String xmlString) {
            this.xmlString = xmlString;
        }

        public String getXmlString() {
            return this.xmlString;
        }

        public static TradeType fromXmlString(String xmlValue) {
            switch (xmlValue.toLowerCase()) {
            case "true":
                return TradeType.BUY;
            case "false":
                return TradeType.SELL;
            default:
                throw new IllegalArgumentException(xmlValue);
            }
        }
    }

    public enum TradeState {
        INIT, WAITING_FOR_REPLY, FILLED, PARTIALLY_FILLED,
        CANCELED, DONE_FOR_DAY, REJECTED;
    }

    private int id = -1;
    private double price;
    private int size;
    private int algoType;
    private int strat;

    // ignore these fields in xml, we will use the getters/setters for xml
    @XmlTransient
    private Stock stock;

    @XmlTransient
    private String tempStockTicker;

    @XmlTransient
    private MovingAverages moving;

    @XmlTransient
    private LocalDateTime lastStateChange = LocalDateTime.now();

    @XmlTransient
    private TradeType tradeType;

    @XmlTransient
    private TradeState state = TradeState.INIT;

    public TradeMoving() {}

    public TradeMoving (Stock stock, double price, int size,
            TradeType tradeType, MovingAverages moving, int algoType, int strat) {
        this(-1, stock, price, size, tradeType,
             TradeState.INIT, LocalDateTime.now(), moving, algoType, strat);
    }
    public TradeMoving (int id, Stock stock, double price, int size,
                 String tradeType, String tradeState, LocalDateTime lastStateChange,
                 MovingAverages moving, int algoType, int strat) {
        this(id, stock, price, size, TradeType.valueOf(tradeType),
             TradeState.valueOf(tradeState), lastStateChange, moving, algoType, strat);
    }

    public TradeMoving(int id, Stock stock, double price, int size,
                 TradeType tradeType, TradeState tradeState,
                 LocalDateTime lastStateChange, MovingAverages moving, int algoType, int strat) {
        this.id = id;
        this.stock = stock;
        this.price = price;
        this.size = size;
        this.moving = moving;
        this.setState(tradeState);
        this.setLastStateChange(lastStateChange);
        this.setTradeType(tradeType);
        this.setAlgoType(algoType);
        this.setStrat(strat);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @XmlElement(name = "stock")
    public String getStockTicker() {
        return stock.getTicker();
    }

    public void setStockTicker(String ticker) {
        this.tempStockTicker = ticker;
    }

    public String getTempStockTicker() {
        return tempStockTicker;
    }

    public TradeType getTradeType() {
        return tradeType;
    }

    public void setTradeType(TradeType tradeType) {
        this.tradeType = tradeType;
    }

    // methods for mashalling/unmarshalling tradeType
    @XmlElement(name = "buy")
    public String getTradeTypeXml() {
        return this.tradeType.getXmlString();
    }

    public void setTradeTypeXml(String tradeTypeStr) {
        this.tradeType = TradeType.fromXmlString(tradeTypeStr);
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public MovingAverages getStrategy() {
        return moving;
    }

    public void setStrategy(MovingAverages moving) {
        this.moving = moving;
    }

    public LocalDateTime getLastStateChange() {
        return lastStateChange;
    }

    public void setLastStateChange(LocalDateTime lastStateChange) {
        this.lastStateChange = lastStateChange;
    }

    // methods for mashalling/unmarshalling lastStateChange
    @XmlElement(name = "whenAsDate")
    public String getLastStateChangeXml() {
        return lastStateChange.toString();
    }

    public void setLastStateChangeXml(String lastStateChange) {
        this.lastStateChange = LocalDateTime.parse(lastStateChange);
    }

    public TradeState getState() {
        return state;
    }

    public void setState(TradeState state) {
        this.state = state;
    }

    // to be called when state is changing (setState used for obj creation etc..)
    public void stateChange(TradeState newState) {
        this.state = newState;
        this.lastStateChange = LocalDateTime.now();
    }

    // methods for mashalling/unmarshalling state
    @XmlElement(name = "result")
    public TradeState getStateXml() {
        // don't include state when marshalling obj=>xml
        return null;
    }

    public void setStateXml(TradeState state) {
        stateChange(state);
    }

    @Override
    public String toString() {
        String returnVal =
               "TradeMoving [id=" + id + ", stock=" + stock + ", tradeType=" + tradeType +
               ", price=" + price + ", size=" + size +
               ", state=" + state + ", lastStateChange=" + lastStateChange +
               ", strategyId=";

        if(moving != null) {
            returnVal += (moving.getId());
        } else {
            returnVal += ("null");
        }
        return returnVal;
    }

    @Override
    public boolean equals(Object trade) {
        if (trade == this) {
            return true;
        }

        // This implementation allows ids to be different
        return trade != null && (trade instanceof TradeMoving) && getStock().equals(((TradeMoving) trade).getStock())
                && getTradeType() == ((TradeMoving) trade).getTradeType() && getPrice() == ((TradeMoving) trade).getPrice()
                && getSize() == ((TradeMoving) trade).getSize() && getState() == ((TradeMoving) trade).getState()
                && getLastStateChange().equals(((TradeMoving) trade).getLastStateChange())
                && getStrategy() == ((TradeMoving) trade).getStrategy();
    }

    /**
     * Converts from Trade object to XML representation.
     * @param trade the Trade object to convert.
     * @return the XML String representation of this trade.
     */
    public static String toXml(TradeMoving trade) {
        StringWriter stringWriter = new StringWriter();

        try {
            marshaller.marshal(trade, stringWriter);
            return stringWriter.toString();
        } catch (JAXBException e) {
            log.error("Unabled to marshall trade: " + trade);
            log.error("JAXBException: " + e);
        }
        return null;
    }

    /**
     * Converts XML representation of trade to Trade object.
     * @param xmlString representation of a trade
     * @return Trade object representation of the trade
     */
    public static TradeMoving fromXml(String xmlString) {
        try {
            return (TradeMoving) unmarshaller.unmarshal(new StringReader(xmlString));
        } catch (JAXBException e) {
            log.error("Unable to unmarshal trade: " + xmlString);
            log.error("JAXBException: " + e);
        }
        return null;
    }

	public int getAlgoType() {
		return algoType;
	}

	public void setAlgoType(int algoType) {
		this.algoType = algoType;
	}

	public int getStrat() {
		return strat;
	}

	public void setStrat(int strat) {
		this.strat = strat;
	}
}