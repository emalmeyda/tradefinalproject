package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.TradeMoving;

public interface TradeMovingDao {
	
	int save(TradeMoving trade);

    List<TradeMoving> findAll();

    TradeMoving findById(int id);

    List<TradeMoving> findAllByState(TradeMoving.TradeState state);

    void deleteById(int id);

}
