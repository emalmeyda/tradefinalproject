package com.citi.training.trader.model;

import java.util.Date;

public class MovingAverages {
	
	 private static final int DEFAULT_MAX_TRADES = 20;

	    private int id;
	    private Stock stock;
	    private int size;
	    private double exitProfitLoss;
	    private int short_ma_seconds;
	    private int long_ma_seconds;
	    private double lastShortMA;
	    private double lastLongMA;
	    private int currentPosition;
	    private double lastTradePrice;
	    private double profit;
	    private int trade_pl;
	    private Date stopped;

	    public MovingAverages(Stock stock, int size) {
	        this(-1, stock, size, DEFAULT_MAX_TRADES, 30, 240, 0, 0, 0, 0, 0, 1, null); // 10 seconds shortma 80 seconds long ma defualr
	    }

	    public MovingAverages(int id, Stock stock, int size,
	                          double exitProfitLoss, int short_ma_seconds, int long_ma_seconds, 
	                          double lastShortMA, double lastLongMA, int currentPosition,
	                          double lastTradePrice, double profit, int trade_pl,
	                          Date stopped) {
	        this.id = id;
	        this.stock = stock;
	        this.size = size;
	        this.exitProfitLoss = exitProfitLoss;
	        this.setShort_ma_seconds(short_ma_seconds);
	        this.setLong_ma_seconds(long_ma_seconds);
	        this.currentPosition = currentPosition;
	        this.lastTradePrice = lastTradePrice;
	        this.lastShortMA = lastShortMA;
	        this.lastLongMA = lastLongMA;
	        this.profit = profit;
	        this.setTrade_pl(trade_pl);
	        this.stopped = stopped;
	    }

	    public int getId() {
	        return id;
	    }

	    public void setId(int id) {
	        this.id = id;
	    }

	    public Stock getStock() {
	        return stock;
	    }

	    public void setStock(Stock stock) {
	        this.stock = stock;
	    }

	    public int getSize() {
	        return size;
	    }

	    public void setSize(int size) {
	        this.size = size;
	    }

	    public double getExitProfitLoss() {
	        return exitProfitLoss;
	    }

	    public void setExitProfitLoss(double exitProfitLoss) {
	        this.exitProfitLoss = exitProfitLoss;
	    }

	    public int getCurrentPosition() {
	        return currentPosition;
	    }

	    public void setCurrentPosition(int currentPosition) {
	        this.currentPosition = currentPosition;
	    }

	    public double getLastTradePrice() {
	        return lastTradePrice;
	    }

	    public void setLastTradePrice(double lastTradePrice) {
	        this.lastTradePrice = lastTradePrice;
	    }

	    public double getProfit() {
	        return profit;
	    }

	    public void setProfit(double profit) {
	        this.profit = profit;
	    }

	    public Date getStopped() {
	        return stopped;
	    }

	    public void setStopped(Date stopped) {
	        this.stopped = stopped;
	    }

	    public void addProfitLoss(double profitLoss) {
	        this.profit += profitLoss;
	        if(Math.abs(this.profit) >= exitProfitLoss) {
	            this.setStopped(new Date());
	        }
	    }

	    public void stop() {
	        this.stopped = new Date();
	    }

	    public boolean hasPosition() {
	        return this.currentPosition != 0;
	    }

	    public boolean hasShortPosition() {
	        return this.currentPosition < 0;
	    }

	    public boolean hasLongPosition() {
	        return this.currentPosition > 0;
	    }

	    public void takeShortPosition() {
	        this.currentPosition = -1;
	    }

	    public void takeLongPosition() {
	        this.currentPosition = 1;
	    }

	    public void closePosition() {
	        this.currentPosition = 0;
	    }

	    @Override
	    public String toString() {
	        return "Strategy [id=" + id + ", stock=" + stock +
	                ", size=" + size + ", maxTrades=" + exitProfitLoss +
	                ", currentPosition=" + currentPosition +
	                ", stopped=" + stopped + "]";
	    }

		public int getShort_ma_seconds() {
			return short_ma_seconds;
		}

		public void setShort_ma_seconds(int short_ma_seconds) {
			this.short_ma_seconds = short_ma_seconds;
		}

		public int getLong_ma_seconds() {
			return long_ma_seconds;
		}

		public void setLong_ma_seconds(int long_ma_seconds) {
			this.long_ma_seconds = long_ma_seconds;
		}

		public double getTrade_pl() {
			return trade_pl;
		}

		public void setTrade_pl(int trade_pl) {
			this.trade_pl = trade_pl;
		}

		public double getLastShortMA() {
			return lastShortMA;
		}

		public void setLastShortMA(double lastShortMA) {
			this.lastShortMA = lastShortMA;
		}

		public double getLastLongMA() {
			return lastLongMA;
		}

		public void setLastLongMA(double lastLongMA) {
			this.lastLongMA = lastLongMA;
		}

		
	

}
