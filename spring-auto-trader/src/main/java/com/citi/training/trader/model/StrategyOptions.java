package com.citi.training.trader.model;

public class StrategyOptions {
	private String strategy;
	private String options;
	private String description;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public StrategyOptions(String strategy, String options, String description) {
		this.strategy = strategy;
		this.options = options;
		this.description = description;
	}
	public String getStrategy() {
		return strategy;
	}
	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}
	public String getOptions() {
		return options;
	}
	public void setOptions(String options) {
		this.options = options;
	}
	
	
}
