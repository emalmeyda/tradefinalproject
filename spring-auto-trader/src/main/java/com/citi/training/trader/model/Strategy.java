package com.citi.training.trader.model;

public class Strategy {
	private String strategy;
	private String displayName;

	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public Strategy(String strategy, String displayName) {
		this.strategy = strategy;
		this.displayName = displayName;
	}
	public String getStrategy() {
		return strategy;
	}
	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}
}
