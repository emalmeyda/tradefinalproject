package com.citi.training.trader.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.StrategyOptionsDao;
import com.citi.training.trader.exceptions.StrategyOptionsNotFoundException;
import com.citi.training.trader.model.StrategyOptions;

@Component
public class MysqlStrategyOptionsDao implements StrategyOptionsDao {
    
    @Autowired
    JdbcTemplate tpl;
    private static final Logger logger = LoggerFactory.getLogger(StrategyOptionsDao.class);
    
    public List<StrategyOptions> findAll(){
    	logger.info("Executing findAll Query");
        return tpl.query("select strategy, options, description from strategyOptions",
                         new StrategyOptionsMapper());
    }

    @Override
    public StrategyOptions findById(String id){
    	logger.info("Executing findID Query");
        List<StrategyOptions> StrategyOptionss = this.tpl.query(
                "select strategy, options, description from strategyOptions where strategy = ?",
                new Object[]{id},
                new StrategyOptionsMapper()
        );
        if(StrategyOptionss.size() <= 0) {
        	logger.error("ID does not exist");
            throw new StrategyOptionsNotFoundException("Not Found");
        }
        logger.info("ID successfully found");
        return StrategyOptionss.get(0);
    }
    /**
     * creates a new strategyOptions to insert to the database
     */
    @Override
    public String create(StrategyOptions StrategyOptions) {
    	logger.info("Executing create Query");
        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.tpl.update(
            new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps =
                            connection.prepareStatement("insert into strategyOptions (strategy, options, description) values (?, ?, ?)",
                            Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, StrategyOptions.getStrategy());
                    ps.setString(2, StrategyOptions.getOptions());
                    ps.setString(3, StrategyOptions.getDescription());
                    return ps;
                }
            },
            keyHolder);
        logger.info("create successful");
        return keyHolder.getKey().toString();
    }
    @Override
    public void deleteById(String id) throws StrategyOptionsNotFoundException {
    	logger.info("Executing delete id");
    	if( this.tpl.update("delete from strategyOptions where strategy=?", id) == 0) {
    		logger.error("id not found");
    		throw new StrategyOptionsNotFoundException("Id can't be deleted, doesn't exist");
    	}
    }
    private static final class StrategyOptionsMapper implements RowMapper<StrategyOptions> {
        public StrategyOptions mapRow(ResultSet rs, int rowNum) throws SQLException {
        	logger.info("Mapping query result");
            return new StrategyOptions(rs.getString("strategy"),
                                rs.getString("options"),
            					rs.getString("description"));
        }
    }
}
