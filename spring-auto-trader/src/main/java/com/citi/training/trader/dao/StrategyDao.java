package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.Strategy;

public interface StrategyDao {
	List<Strategy> findAll();
	/*int create(Strategy Strategy);
	Strategy findById(int id);
	void deleteById(int id);*/
}