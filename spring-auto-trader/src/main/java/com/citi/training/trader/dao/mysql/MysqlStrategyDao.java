package com.citi.training.trader.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.model.Strategy;

@Component
public class MysqlStrategyDao implements StrategyDao {
    
    @Autowired
    JdbcTemplate tpl;
    private static final Logger logger = LoggerFactory.getLogger(StrategyDao.class);
    public List<Strategy> findAll(){
    	logger.info("Executing findAll Query");
        return tpl.query("select strategy, displayName from strategies",
                         new StrategyMapper());
    }

    private static final class StrategyMapper implements RowMapper<Strategy> {
        public Strategy mapRow(ResultSet rs, int rowNum) throws SQLException {
        	logger.info("Mapping query result");
            return new Strategy(rs.getString("strategy"), rs.getString("displayName"));
        }
    }
}
