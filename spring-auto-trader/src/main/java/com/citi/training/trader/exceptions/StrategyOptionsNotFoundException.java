package com.citi.training.trader.exceptions;

@SuppressWarnings("serial")
public class StrategyOptionsNotFoundException extends RuntimeException {
    public StrategyOptionsNotFoundException(String msg) {
        super(msg);
    }
}
