package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.StrategyStateDao;
import com.citi.training.trader.model.MovingAverages;
import com.citi.training.trader.model.SimpleStrategy;

@Component
public class StrategyStateService {

	@Autowired
	private StrategyStateDao strategyStateDao;
	
	public List<SimpleStrategy> getPL() {
		return strategyStateDao.getPL();
	}
	
	public List<MovingAverages> getPLMA() {
		return strategyStateDao.getPLMA();
	}
}
