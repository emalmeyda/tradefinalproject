package com.citi.training.trader.strategy;

public interface MovingAlgorithm {
	
	public void run();

}
