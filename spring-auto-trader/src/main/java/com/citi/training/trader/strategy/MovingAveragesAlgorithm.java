package com.citi.training.trader.strategy;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.MovingAveragesDao;
import com.citi.training.trader.dao.PriceDao;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.MovingAverages;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.TradeMoving;

@Component
public class MovingAveragesAlgorithm implements MovingAlgorithm {

	private static final Logger logger =
            LoggerFactory.getLogger(MovingAveragesAlgorithm.class);
	
	private static String GET_TRADE_STATUS = "select state from trade where id = (select max(id) from trade)";

	@Autowired
	private PriceDao priceDao;
	
	@Autowired
	private TradeSender tradeSender;
	
	@Autowired
    private JdbcTemplate tpl;
	
	@Autowired
	private MovingAveragesDao movingDao;
	
	// this refreshes every 1 second
	@Scheduled(fixedRateString = "${moving.averages.refresh.rate_ms:5000}")
    public void run() {
    	
        for(MovingAverages moving: movingDao.findAll()) {
            if(moving.getStopped() != null) {
                continue;
            }
            
            //System.out.println("we have started");
                        
            if (priceDao.timeDiff(moving.getStock()) < moving.getLong_ma_seconds()) {
            	//System.out.println("Not enough prices to do rolling average");
            	continue;
            }
            else {
            	
            	moving.setLastShortMA(Math.round(priceDao.getRolling(moving.getShort_ma_seconds()) * 1000.0) / 1000.0);
        		moving.setLastLongMA(Math.round(priceDao.getRolling(moving.getLong_ma_seconds()) * 1000.0) / 1000.0);
        		
        		
            	if (!moving.hasPosition()) {
            		
            		// get rolling means
            		
            		// check to make a long position
            		if (moving.getLastShortMA() > moving.getLastLongMA()) {
            			// take a long position
            			
            			//System.out.println("Buying stock");
            			
                        logger.debug("Taking long position for moving averages: " + moving);
            			moving.takeLongPosition();
                        moving.setLastTradePrice(makeTrade(moving, TradeMoving.TradeType.BUY));
            		}
            		else if (moving.getLastShortMA() < moving.getLastLongMA()) {
            			// take short position
            			
            			//System.out.println("Shorting stock");
            			
                        logger.debug("Taking short position for strategy: " + moving);
                        moving.takeShortPosition();
                        moving.setLastTradePrice(makeTrade(moving, TradeMoving.TradeType.SELL));
            		}
            		else {
            			// take no position because there wasn't a trade moving average difference
            			;
            		}
            		
            		
            	}
            	// now currently there is a long position
            	else if (moving.hasLongPosition()) {
            		
            		// get last price minus traded price
            		//if (((priceDao.getRecentPrice(moving.getStock()) - moving.getLastTradePrice()) * moving.getSize()) >= moving.getTrade_pl()) {
            		if (((moving.getLastShortMA() - moving.getLastLongMA()) * moving.getSize()) >= moving.getTrade_pl()) {	
            			// close long because of temp profit was made
            			// close the position by selling
            			
            			//System.out.println("Selling because of trade pl met");

            			
                        logger.debug("Closing long position for moving: " + moving);

                        double thisTradePrice = makeTrade(moving, TradeMoving.TradeType.SELL);
                        logger.debug("Bought at: " + moving.getLastTradePrice() + ", sold at: " +
                                     thisTradePrice);
                        closePosition(thisTradePrice - moving.getLastTradePrice(), moving);

            		}
            		// (priceDao.getRecentPrice(moving.getStock()) - moving.getLastTradePrice()) <= 0.0)
            		else if ((moving.getLastShortMA() - moving.getLastLongMA()) <= 0.0) {
            			// close because of break even and stock started to move down
            			// close the position by selling
            			
            			//System.out.println("Sold because long stock is moving down");
           			
                        logger.debug("Closing long position for moving: " + moving);

                        double thisTradePrice = makeTrade(moving, TradeMoving.TradeType.SELL);
                        logger.debug("Bought at: " + moving.getLastTradePrice() + ", sold at: " +
                                     thisTradePrice);
                        closePosition(thisTradePrice - moving.getLastTradePrice(), moving);
            		}
            		
            	}
            	else if (moving.hasShortPosition()) {
            		//if (((moving.getLastTradePrice() - priceDao.getRecentPrice(moving.getStock())) * moving.getSize()) >= moving.getTrade_pl()) {
            		if (((moving.getLastLongMA() - moving.getLastShortMA()) * moving.getSize()) >= moving.getTrade_pl()) {
            		// close short because of temp profit was made
            			
            			//System.out.println("closing short because trade pl met");
            			
            			// close the position by buying
                        logger.debug("Closing short position for moving: " + moving);

                        double thisTradePrice = makeTrade(moving, TradeMoving.TradeType.BUY);
                        logger.debug("Sold at: " + moving.getLastTradePrice() + ", bought at: " +
                                     thisTradePrice);

                        closePosition(moving.getLastTradePrice() - thisTradePrice, moving);
            		}
            		// ((moving.getLastTradePrice() - priceDao.getRecentPrice(moving.getStock())) <= 0.0) 
            		else if ((moving.getLastLongMA() - moving.getLastShortMA()) <= 0.0) {
            			// close the position by buying
            			
            			//System.out.println("closing short becuase stock is moving up");
            			
                        logger.debug("Closing short position for moving: " + moving);

                        double thisTradePrice = makeTrade(moving, TradeMoving.TradeType.BUY);
                        logger.debug("Sold at: " + moving.getLastTradePrice() + ", bought at: " +
                                     thisTradePrice);

                        closePosition(moving.getLastTradePrice() - thisTradePrice, moving);
            		}
            	}
            	else {
        			//System.out.println("no change in position");

            	}
           
            }  
         
            if (!tpl.queryForObject(GET_TRADE_STATUS, String.class).equals("REJECTED")) {
                //System.out.println("we are not rejected");
            	movingDao.save(moving);
            }
            else
            {
            	logger.debug("we are rejected sorry");
            }
        }
    }

    private void closePosition(double profitLoss, MovingAverages moving) {
        logger.debug("Recording profit/loss of: " + profitLoss +
                     " for moving: " + moving);
        moving.addProfitLoss(profitLoss);
        moving.closePosition();
    }


    private double makeTrade(MovingAverages moving, TradeMoving.TradeType tradeType) {
        Price currentPrice = priceDao.findLatest(moving.getStock(), 1).get(0);
        tradeSender.sendTrade(new TradeMoving(moving.getStock(), currentPrice.getPrice(),
                                        moving.getSize(), tradeType,
                                        moving, moving.getId(), 1));
        
        
        return currentPrice.getPrice();
    }

}
