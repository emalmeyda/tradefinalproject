package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.model.Strategy;

@Component
public class StrategyService {
	@Autowired
	StrategyDao strategyDao;
	
	public List<Strategy> findAll(){
		return strategyDao.findAll();
	}
}
