package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.service.StrategyService;

@RestController
@RequestMapping("${com.citi.training.trader.rest.trade-base-path:/strategy}")
@CrossOrigin("*")
public class StrategyController {

    @Autowired
    StrategyService StrategyService;
    
    private static final Logger logger = LoggerFactory.getLogger(StrategyDao.class);
	/**
	 * REST GET in /trade/ url. Returns all trades performed.
	 * @return returns a list of trades
	 */
    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Strategy> findAll() {
    	logger.info("Running GET method findAll");
        return StrategyService.findAll();
    }
}
