package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.dao.StrategyOptionsDao;
import com.citi.training.trader.model.StrategyOptions;
import com.citi.training.trader.service.StrategyOptionsService;

@RestController
@RequestMapping("${com.citi.training.trader.rest.trade-base-path:/strategyOptions}")
@CrossOrigin(origins = "http://localhost:4200")
public class StrategyOptionsController {

    @Autowired
    StrategyOptionsService StrategyOptionsService;
    
    private static final Logger logger = LoggerFactory.getLogger(StrategyOptionsDao.class);
    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<StrategyOptions> findAll() {
    	logger.info("Running GET method findAll");
        return StrategyOptionsService.findAll();
    }
    
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
    public StrategyOptions findById(@PathVariable String id) {
    	logger.info("Running GET method findID");
        return StrategyOptionsService.findById(id);
    }

    @RequestMapping(method=RequestMethod.POST,
                    consumes=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<StrategyOptions> create(@RequestBody StrategyOptions trade) {
    	logger.info("Running POST method create");
        trade.setStrategy(StrategyOptionsService.create(trade));
        return new ResponseEntity<StrategyOptions>(trade, HttpStatus.CREATED);
    }

    /**
     * DELETE method for deleting id, if id does not exist returns 404 message
     * @param id integer
     */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) {
    	logger.info("Running DELETE method deleteId");
        StrategyOptionsService.deleteById(id);
    }
}
