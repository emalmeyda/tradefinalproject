CREATE TABLE stock
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `ticker` VARCHAR(8) NOT NULL,
  UNIQUE(`ticker`),
  PRIMARY KEY (`id`)
);

CREATE TABLE `trade`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `stock_id` INT NOT NULL,
  `price` DOUBLE NOT NULL,
  `size` INT NOT NULL,
  `buy` BOOLEAN NOT NULL,
  `state` VARCHAR(20),
  `algo` VARCHAR(30),
  `strategy` VARCHAR(30),
  `last_state_change` DATETIME NOT NULL,
  `created` DATETIME NOT NULL,
  CONSTRAINT `trade_stock_foreign_key`
    FOREIGN KEY (`stock_id`) REFERENCES stock (`id`),
  PRIMARY KEY (`id`)
);

CREATE TABLE `simple_strategy`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `stock_id` INT NOT NULL,
  `size` INT NOT NULL,
  `exit_profit_loss` DOUBLE NOT NULL,
  `current_position` INT DEFAULT 0,
  `last_trade_price` DOUBLE,
  `profit` DOUBLE,
  `stopped` DATETIME,
  CONSTRAINT `strategy_stock_foreign_key`
    FOREIGN KEY (`stock_id`) REFERENCES stock (`id`),
  PRIMARY KEY (`id`)
);

CREATE TABLE `moving_averages`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `stock_id` INT NOT NULL,
  `size` INT NOT NULL,
  `exit_profit_loss` DOUBLE NOT NULL,
  `short_ma_seconds` INT NOT NULL,
  `long_ma_seconds` INT NOT NULL,
  `lastShortMA` DOUBLE,
  `lastLongMA` DOUBLE,
  `current_position` INT DEFAULT 0,
  `last_trade_price` DOUBLE,
  `profit` DOUBLE,
  `trade_pl` DOUBLE,
  `stopped` DATETIME,
  CONSTRAINT `strategy_stock_ma_foreign_key`
    FOREIGN KEY (`stock_id`) REFERENCES stock (`id`),
  PRIMARY KEY (`id`)
);

CREATE TABLE price
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `stock_id` INT NOT NULL,
  `price` DOUBLE,
  `recorded_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `price_stock`
    FOREIGN KEY (`stock_id`) REFERENCES stock (`id`)
);


CREATE TABLE strategies
(
  `strategy` VARCHAR(64) NOT NULL,
  `displayName` VARCHAR(64) NOT NULL,
  UNIQUE(`strategy`),
  PRIMARY KEY (`strategy`)
);
CREATE TABLE strategyOptions
(
  `strategy` VARCHAR(64) NOT NULL,
  `options` VARCHAR(512) NOT NULL,
  `description` VARCHAR(1024) NOT NULL,
  UNIQUE(`strategy`),
  PRIMARY KEY (`strategy`)
);

INSERT INTO strategies (strategy, displayName) VALUES ('simple_strategy','Simple');
INSERT INTO strategies (strategy, displayName ) VALUES ('moving_averages','Moving Averages');

INSERT INTO strategyOptions (strategy, options, description) VALUES ('simple_strategy', 'Exit profit loss', '');


INSERT INTO strategyOptions (strategy, options, description) VALUES ('moving_averages', 'Exit profit loss,Short moving average (seconds),Long moving average (seconds),Trade profit loss', 'This trading strategy seeks to take advantage of short-term trends in a stock’s price.

Gather a “long average” of the market price (either the price as reported by Yahoo!, or
the average of bid and ask if price is not available) – that is, a moving average over
some defined period, say 4 hours. Also compute a “short average” over a period of more
like 30 minutes. Continually updating these averages will allow you to observe trends in
the averages themselves.
The trigger for this strategy is when the short average “crosses” the long average: that
is, when it was below the long average a moment ago, and now it is above the long
average, or vice-versa. When the short average crosses above the long, it’s time to buy;
if it crosses below, it’s time to sell. Place your trade and record it. ');
